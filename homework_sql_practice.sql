-- Write a query to display: 
-- 1. the first name, last name, department number, and department name for each employee.
SELECT first_name, last_name, department_id, department_name
FROM employees
LEFT JOIN departments USING(department_id);

-- 2. the first and last name, department, city, and state province for each employee.
SELECT first_name, last_name, department_name, city, state_province
FROM employees
LEFT JOIN departments USING(department_id)
LEFT JOIN locations USING(location_id);

-- 3. the first name, last name, salary, and job grade for all employees.
SELECT first_name, last_name, salary, job_title
FROM employees
LEFT JOIN jobs USING(job_id);

-- 4. the first name, last name, department number and department name, for all employees for departments 80 or 40.
SELECT first_name, last_name, department_id, department_name
FROM employees
LEFT JOIN departments USING(department_id)
WHERE department_id = 40 OR department_id = 80;

-- 5. those employees who contain a letter z to their first name and also display their last name, department, city, and state province.
SELECT first_name, last_name, department_name, city, state_province
FROM employees
LEFT JOIN departments USING(department_id)
LEFT JOIN locations USING(location_id)
WHERE lower(first_name) LIKE '%z%';

-- 6. all departments including those where does not have any employee.
SELECT department_name FROM departments;

-- 7. the first and last name and salary for those employees who earn less than the employee earn whose number is 182.
SELECT first_name, last_name, salary
FROM employees
WHERE salary < (SELECT salary FROM employees WHERE employee_id = 182);

-- 8. the first name of all employees including the first name of their manager.
SELECT e.first_name, m.first_name
FROM employees e
INNER JOIN employees m
ON e.manager_id = m.employee_id;

-- 9. the department name, city, and state province for each department.
SELECT department_name, city, state_province
FROM departments
INNER JOIN locations USING(location_id);

--10. the first name, last name, department number and name, for all employees who have or have not any department.
SELECT first_name, last_name, department_id, department_name
FROM employees e
LEFT JOIN departments USING(department_id);

--11. the first name of all employees and the first name of their manager including those who does not working under any manager.
SELECT e.first_name, m.first_name
FROM employees e
LEFT JOIN employees m
ON e.manager_id = m.employee_id;

--12. the first name, last name, and department number for those employees who works in the same department as the employee who holds the last name as Taylor.
SELECT first_name, last_name, department_id
FROM employees
INNER JOIN departments USING(department_id)
WHERE department_id IN (SELECT department_id FROM employees WHERE last_name = 'Taylor');

--13. the job title, department name, full name (first and last name ) of employee, and starting date for all the jobs which started on or after 1st January, 1993 and ending with on or before 31 August, 1997.
SELECT job_title, department_name, first_name || ' ' || last_name full_name, start_date, end_date
FROM employees e LEFT JOIN jobs USING(job_id)
LEFT JOIN departments USING(department_id)
LEFT JOIN job_history h ON (e.employee_id = h.employee_id AND start_date >= '1-JAN-93' AND end_date <= '31-AUG-97');
-- All start_date and end_date is equal to null, because there are no employees that started after and ended before written date
-- can use inner, but the resul will be empty
-- a little bit missunderstanding start_date and hiring_date

--14. job title, full name (first and last name ) of employee, and the difference between maximum salary for the job and salary of the employee.
SELECT job_title, first_name || ' ' || last_name full_name, max_salary - salary max_salary_difference
FROM employees
INNER JOIN jobs USING(job_id);

--15. the name of the department, average salary and number of employees working in that department who got commission.
SELECT department_name, to_char(avg(salary), '9999D99') salary_average, count(employee_id) employees_nr
FROM employees
INNER JOIN departments USING(department_id)
WHERE commission_pct IS NOT NULL
GROUP BY department_name;
-- to_char looks right for float, but displays #### for int

--16. the full name (first and last name ) of employee, and job title of those employees who is working in the department which ID is 80.
SELECT first_name || ' ' || last_name full_name, job_title
FROM employees e
INNER JOIN jobs j ON (e.job_id = j.job_id AND e.department_id = 80);

--SELECT first_name || ' ' || last_name full_name, job_title
--FROM employees
--INNER JOIN jobs USING(job_id)
--WHERE department_id = 80;

-- what is a better option ?

--17. the name of the country, city, and the departments which are running there.
SELECT country_name, city, department_name
FROM locations
INNER JOIN countries USING(country_id)
INNER JOIN departments USING(location_id);

----18. department name and the full name (first and last name) of the manager.
SELECT department_name, first_name || ' ' || last_name full_name
FROM departments d
INNER JOIN employees e ON d.manager_id = e.employee_id;

--19. job title and average salary of employees.
SELECT job_title, avg(salary) average_salary
FROM jobs
INNER JOIN employees USING(job_id)
GROUP BY job_title;

--20. the details of jobs which was done by any of the employees who is presently earning a salary on and above 12000.
SELECT job_title, min_salary, max_salary
FROM jobs
INNER JOIN employees USING (job_id)
WHERE salary >= 12000;

--21. the country name, city, and number of those departments where at leaste 2 employees are working.
SELECT country_name, city, department_id
FROM locations
INNER JOIN countries USING (country_id)
INNER JOIN departments using(location_id)
WHERE department_id IN (
  SELECT department_id FROM departments
  INNER JOIN employees USING (department_id)
  GROUP BY department_id
  HAVING count(employee_id) >= 2
);

--22. the department name, full name (first and last name) of manager, and their city.
SELECT department_name, first_name || ' ' || last_name full_name, city
FROM departments d
INNER JOIN employees e ON d.manager_id = e.employee_id
INNER JOIN locations USING (location_id);

--23. the employee ID, job name, number of days worked in for all those jobs in department 80.
SELECT employee_id, job_title, end_date - start_date number_of_days
FROM employees e
INNER JOIN jobs USING(job_id)
INNER JOIN job_history USING(employee_id)
WHERE e.department_id = 80;

--SELECT employee_id, job_title, CAST(CURRENT_DATE - hire_date AS INT) number_of_days
--FROM employees e
--INNER JOIN jobs USING(job_id)
--WHERE department_id = 80;
-- not sure what to use end date - start date from job history or current date - hire date from employee

--24. the full name (first and last name), and salary of those employees who working in any department located in London.
SELECT first_name || ' ' || last_name full_name, salary
FROM employees
INNER JOIN departments USING(department_id)
INNER JOIN locations USING(location_id)
WHERE city = 'London';

--SELECT first_name || ' ' || last_name full_name, salary
--FROM employees e
--INNER JOIN departments d USING(department_id)
--INNER JOIN locations l ON d.location_id = l.location_id AND city = 'London';

-- not sure what whay is better

--25. full name(first and last name), job title, starting and ending date of last jobs for those employees with worked without a commission percentage.
SELECT DISTINCT first_name || ' ' || last_name full_name, job_title, h.start_date, end_date
FROM employees e
INNER JOIN jobs USING(job_id)
INNER JOIN job_history h ON e.employee_id = h.employee_id
INNER JOIN (
  SELECT employee_id, max(start_date) start_date
  FROM job_history
  GROUP BY employee_id
) m ON h.start_date = m.start_date
WHERE commission_pct IS NULL
ORDER BY full_name;

--26. the department name and number of employees in each of the department.
SELECT department_name, count(employee_id) employees_nr
FROM departments
LEFT JOIN employees USING(department_id)
GROUP BY department_name;

--27. the full name (firt and last name ) of employee with ID and name of the country presently where (s)he is working.
SELECT first_name || ' ' || last_name full_name, employee_id, country_name
FROM employees
INNER JOIN departments USING(department_id)
INNER JOIN locations USING(location_id)
INNER JOIN countries USING(country_id)
INNER JOIN job_history USING(employee_ID)
WHERE end_date IS NULL;
-- don't understand what defines that a person is currently working

--28. the name ( first name and last name ) for those employees who gets more salary than the employee whose ID is 163.
SELECT first_name || ' ' || last_name full_name, salary
FROM employees
WHERE salary > (
  SELECT salary FROM employees WHERE employee_id = 163
);

--SELECT e.first_name || ' ' || e.last_name full_name, e.salary
--FROM employees e
--INNER JOIN employees e2 ON (e2.employee_id = 163 AND e.salary > e2.salary);

-- not sure what is better to use

--29. the name ( first name and last name ), salary, department id, job id for those employees who works in the same designation as the employee works whose id is 169.
SELECT first_name || ' ' || last_name full_name, salary, department_id, job_id
FROM employees
INNER JOIN jobs USING(job_id)
INNER JOIN departments USING(department_id)
INNER JOIN locations USING(location_id)
WHERE state_province = (
  SELECT state_province FROM locations
  INNER JOIN departments USING(location_id)
  INNER JOIN employees USING(department_id)
  WHERE employee_id = 169
);
-- what is designation ? used state_province for it

--30. the name ( first name and last name ), salary, department id for those employees who earn such amount of salary which is the smallest salary of any of the departments.
-- don't understand what to do

--31. the employee id, employee name (first name and last name ) for all employees who earn more than the average salary.
SELECT employee_id, first_name || ' ' || last_name full_name
FROM employees
WHERE salary > (
  SELECT avg(salary)
  FROM employees
);

--SELECT employee_id, first_name || ' ' || last_name full_name
--FROM employees e
--INNER JOIN (SELECT avg(salary) salary FROM employees) a ON e.salary > a.salary;

-- not sure what is better

--32. the employee name ( first name and last name ), employee id and salary of all employees who report to Payam. 
SELECT first_name || ' ' || last_name full_name, employee_id, salary
FROM employees
WHERE manager_id = (
  SELECT employee_id FROM employees WHERE first_name = 'Payam'
);

--33. the department number, name ( first name and last name ), job and department name for all employees in the Finance department.
SELECT department_id, first_name || ' ' || last_name full_name, job_title, department_name
FROM departments
INNER JOIN employees USING (department_id)
INNER JOIN jobs USING (job_id)
WHERE department_name = 'Finance';

--34. all the information of an employee whose salary and reporting person id is 3000 and 121 respectively.
SELECT employee_id, first_name || ' ' || last_name full_name, email, phone_number, salary, hire_date
FROM employees
WHERE salary = 3000 AND manager_id = 121;

--35. all the information of an employee whose id is any of the number 134, 159 and 183.   Go to the editor 
SELECT employee_id, first_name || ' ' || last_name full_name, email, phone_number, salary, hire_date
FROM employees
WHERE employee_id IN (134, 159, 183);

--36. all the information of the employees whose salary is within the range 1000 and 3000.
SELECT employee_id, first_name || ' ' || last_name full_name, email, phone_number, salary, hire_date
FROM employees
WHERE salary BETWEEN 1000 AND 3000;

--37. all the information of the employees whose salary is within the range of smallest salary and 2500. 
SELECT employee_id, first_name || ' ' || last_name full_name, email, phone_number, salary, hire_date
FROM employees
WHERE salary BETWEEN (SELECT MIN(salary) FROM employees) AND 2500;

--38. all the information of the employees who does not work in those departments where some employees works whose id within the range 100 and 200.
SELECT employee_id, first_name || ' ' || last_name full_name, email, phone_number, salary, hire_date
FROM employees
WHERE department_id NOT IN (
  SELECT DISTINCT department_id
  FROM employees
  WHERE employee_id BETWEEN 100 AND 200 AND department_id IS NOT NULL
);

--39. all the information for those employees whose id is any id who earn the second highest salary. 
SELECT employee_id, first_name || ' ' || last_name full_name, email, phone_number, salary, hire_date
FROM employees
ORDER BY salary DESC
OFFSET 1 ROWS FETCH NEXT 1 ROWS ONLY;
-- selected info about an employee with the second highest salary

--40. the employee name( first name and last name ) and hiredate for all employees in the same department as Clara. Exclude Clara.
SELECT first_name || ' ' || last_name full_name, hire_date
FROM employees e
INNER JOIN departments d ON e.department_id = d.department_id
INNER JOIN (
  SELECT employee_id, department_id
  FROM employees
  WHERE first_name = 'Clara'
) clara ON d.department_id = clara.department_id AND e.employee_id != clara.employee_id;
-- not sure if it is a good implementation

--41. the employee number and name( first name and last name ) for all employees who work in a department with any employee whose name contains a T. 
SELECT employee_id, first_name || ' ' || last_name full_name
FROM employees e
INNER JOIN (
  SELECT DISTINCT department_id
  FROM departments
  INNER JOIN employees USING(department_id)
  WHERE first_name LIKE '%T%' OR last_name LIKE '%T%'
) d2 ON e.department_id = d2.department_id;

--42. the employee number, name( first name and last name ), and salary for all employees who earn more than the average salary and who work in a department with any employee with a J in their name. 
SELECT employee_id, first_name || ' ' || last_name full_name, salary
FROM employees e
INNER JOIN departments d ON e.department_id = d.department_id
INNER JOIN (
  SELECT DISTINCT department_id
  FROM departments
  INNER JOIN employees USING(department_id)
  WHERE first_name LIKE '%J%' OR last_name LIKE '%J%'
) d2 ON d.department_id = d2.department_id
WHERE salary > (SELECT avg(salary) FROM employees);

--43. the employee name( first name and last name ), employee id, and job title for all employees whose department location is Toronto. 
SELECT first_name || ' ' || last_name full_name, employee_id, job_title
FROM employees
INNER JOIN jobs USING(job_id)
INNER JOIN departments USING(department_id)
INNER JOIN locations USING(location_id)
WHERE city = 'Toronto';

--44. the employee number, name( first name and last name ) and job title for all employees whose salary is smaller than any salary of those employees whose job title is MK_MAN.  
SELECT employee_id, first_name || ' ' || last_name full_name, job_title
FROM employees
INNER JOIN jobs USING(job_id)
WHERE SALARY <= (
  SELECT salary
  FROM employees
  INNER JOIN jobs USING(job_id)
  WHERE job_id = 'MK_MAN'
);
-- here should be <= and we will get MK_MAN and the next query will have sense

--45. the employee number, name( first name and last name ) and job title for all employees whose salary is smaller than any salary of those employees whose job title is MK_MAN. Exclude Job title MK_MAN.  
SELECT e.employee_id, first_name || ' ' || last_name full_name, job_title
FROM employees e
INNER JOIN jobs USING(job_id)
INNER JOIN (
  SELECT employee_id, salary
  FROM employees
  INNER JOIN jobs USING(job_id)
  WHERE job_id = 'MK_MAN'
) s ON e.salary < s.salary AND e.employee_id != s.employee_id;

--46. all the information of those employees who did not have any job in the past.
SELECT employee_id, first_name || ' ' || last_name full_name, email, phone_number, salary, hire_date
FROM employees
LEFT JOIN job_history USING(employee_id)
WHERE start_date IS NULL;

--47. the employee number, name( first name and last name ) and job title for all employees whose salary is more than any average salary of any department.  
SELECT employee_id, first_name || ' ' || last_name full_name, job_title
FROM employees e
INNER JOIN jobs USING(job_id)
INNER JOIN (
  SELECT department_id, avg(salary) salary
  FROM employees
  GROUP BY department_id
  HAVING department_id IS NOT NULL
) a ON e.department_id = a.department_id AND e.salary > a.salary;
-- selected all employes from all departments that have salary bigger than it's department's average

--48. the employee name( first name and last name ) and department for all employees for any existence of those employees whose salary is more than 3700.  
SELECT first_name || ' ' || last_name full_name, department_name
FROM employees
INNER JOIN departments USING(department_id)
WHERE salary > 3700;
-- selected info about employees with a salary > 3700

--49. the department id and the total salary for those departments which contains at least one salaried employee.   
SELECT department_id, sum(salary) total_salary
FROM departments
INNER JOIN employees USING(department_id)
GROUP BY department_id;

--50. the employee id, name ( first name and last name ) and the job id column with a modified title SALESMAN for those employees whose job title is ST_MAN and DEVELOPER for whose job title is IT_PROG.  
SELECT employee_id, first_name || ' ' || last_name full_name,
CASE
  WHEN job_id = 'IT_PROG' THEN 'DEVELOPER'
  WHEN job_id = 'ST_MAN' THEN 'SALESMAN'
  ELSE job_id 
END AS job_id 
FROM employees
INNER JOIN jobs USING(job_id);

--51. the employee id, name ( first name and last name ), salary and the SalaryStatus column with a title HIGH and LOW respectively for those employees whose salary is more than and less than the average salary of all employees.  
SELECT employee_id, first_name || ' ' || last_name full_name, salary,
CASE
  WHEN salary > (SELECT avg(salary) FROM employees) THEN 'HIGH'
  WHEN salary < (SELECT avg(salary) FROM employees) THEN 'LOW'
END AS salary_status 
FROM employees;

--52. the employee id, name ( first name and last name ), SalaryDrawn, AvgCompare (salary - the average salary of all employees)
-- and the SalaryStatus column with a title HIGH and LOW respectively for those employees whose salary is more than and less than
-- the average salary of all employees.  
SELECT employee_id, first_name || ' ' || last_name full_name,
  to_char(salary - (SELECT avg(salary) FROM employees), '9999D99') average_compare, job_title,
  CASE
    WHEN salary > (SELECT avg(salary) FROM employees) THEN 'HIGH'
    WHEN salary < (SELECT avg(salary) FROM employees) THEN 'LOW'
  END AS salary_status 
FROM employees
INNER JOIN jobs USING(job_id)
WHERE salary < (SELECT avg(salary) FROM employees) OR salary > (SELECT avg(salary) FROM employees);
-- what is salary drawn ?
-- to_char for int ??

--53. a set of rows to find all departments that do actually have one or more employees assigned to them.   
SELECT DISTINCT department_name
FROM departments
INNER JOIN employees USING(department_id);

--54. all employees who work in departments located in the United Kingdom.   
SELECT employee_id, first_name || ' ' || last_name full_name, email, phone_number, salary, hire_date
FROM employees
INNER JOIN departments USING(department_id)
INNER JOIN locations USING(location_id)
INNER JOIN countries USING (country_id)
WHERE country_name = 'United Kingdom';

--55. all the employees who earn more than the average and who work in any of the IT departments.   
SELECT employee_id, first_name || ' ' || last_name full_name, email, phone_number, salary, hire_date
FROM employees e
INNER JOIN departments USING(department_id)
WHERE salary > (SELECT avg(salary) FROM employees) AND department_name = 'IT';

--56. who earns more than Mr. Ozer.  
SELECT employee_id, first_name || ' ' || last_name full_name, email, phone_number, salary, hire_date
FROM employees e
WHERE salary > (
  SELECT salary
  FROM employees
  WHERE last_name = 'Ozer'
);

--57. which employees have a manager who works for a department based in the US.    
SELECT employee_id, first_name || ' ' || last_name full_name, email, phone_number, salary, hire_date
FROM employees e
WHERE manager_id IN (
  SELECT DISTINCT e.manager_id
  FROM employees e
  INNER JOIN departments USING(department_id)
  INNER JOIN locations USING (location_id)
  INNER JOIN countries c USING (country_id)
  WHERE c.country_name = 'United States of America' AND e.manager_id IS NOT NULL
);

--58. the names of all employees whose salary is greater than 50% of their department’s total salary bill.  
SELECT first_name || ' ' || last_name full_name
FROM employees e
INNER JOIN (
  SELECT department_id, sum(salary) / 2 salary
  FROM employees
  INNER JOIN departments USING(department_id)
  GROUP BY department_id
) s ON e.department_id = s.department_id AND e.salary > s.salary;

--59. the details of employees who are managers.
SELECT DISTINCT e.employee_id, e.first_name || ' ' || e.last_name full_name, e.email, e.phone_number, e.salary, e.hire_date
FROM employees e
INNER JOIN employees e2 ON e.employee_id = e2.manager_id;

--60. the details of employees who manage a department.   
SELECT employee_id, first_name || ' ' || last_name full_name, email, phone_number, salary, hire_date
FROM employees e
INNER JOIN departments d ON e.employee_id = d.manager_id;

--61. the employee id, name ( first name and last name ), salary, department name and city for all 
--the employees who gets the salary as the salary earn by the employee which is maximum within the joining person January 1st, 2002 and December 31st, 2003.  
SELECT employee_id, first_name || ' ' || last_name full_name, salary, department_name, city
FROM employees
INNER JOIN departments USING(department_id)
INNER JOIN locations USING(location_id)
WHERE salary = (
  SELECT max(salary)
  FROM employees
  WHERE hire_date > '1-JAN-02' AND hire_date < '31-DEC-03'
);

--62. the department code and name for all departments which located in the city London.  
SELECT department_id, department_name
FROM departments
INNER JOIN locations USING(location_id)
WHERE city = 'London';

--63. the first and last name, salary, and department ID for all those employees who earn more than the average salary and arrange the list in descending order on salary. 
SELECT first_name || ' ' || last_name full_name, salary, department_id
FROM employees
WHERE salary > (
  SELECT avg(salary) FROM employees
)
ORDER BY salary DESC;

--64. the first and last name, salary, and department ID for those employees who earn more than the maximum salary of a department which ID is 40. 
SELECT first_name || ' ' || last_name full_name, salary, department_id
FROM employees
WHERE salary > (
  SELECT max(salary)
  FROM employees
  INNER JOIN departments USING(department_id)
  WHERE department_id = 40
);

--65. the department name and Id for all departments where they located, that Id is equal to the Id for the location where department number 30 is located. 
SELECT department_name, department_id
FROM departments
INNER JOIN locations USING(location_id)
WHERE location_id = (
  SELECT location_id
  FROM locations
  INNER JOIN departments USING(location_id)
  WHERE department_id = 30
);

--66. the first and last name, salary, and department ID for all those employees who work in that department where the employee works who hold the ID 201. 
SELECT first_name || ' ' || last_name full_name, salary, department_id
FROM employees
WHERE department_id = (
  SELECT department_id
  FROM employees
  WHERE employee_id = 201
);

--67. the first and last name, salary, and department ID for those employees whose salary is equal to the salary of the employee who works in that department which ID is 40. 
SELECT first_name || ' ' || last_name full_name, salary, department_id
FROM employees
INNER JOIN departments USING(department_id)
WHERE salary = (
  SELECT salary
  FROM employees
  WHERE department_id = 40
);

--68. the first and last name, and department code for all employees who work in the department Marketing.  
SELECT first_name || ' ' || last_name full_name, department_id
FROM employees
INNER JOIN departments USING(department_id)
WHERE department_name = 'Marketing';

--69. the first and last name, salary, and department ID for those employees who earn more than the minimum salary of a department which ID is 40. 
SELECT first_name || ' ' || last_name full_name, salary, department_id
FROM employees
INNER JOIN departments USING(department_id)
WHERE salary > (
  SELECT min(salary)
  FROM employees
  WHERE department_id = 40
);

--70. the full name,email, and designation for all those employees who was hired after the employee whose ID is 165. 
SELECT first_name || ' ' || last_name full_name, email
FROM employees
WHERE hire_date > (
  SELECT hire_date
  FROM employees
  WHERE employee_id = 165
);
-- what is designation ? didn't select designation

--71. the first and last name, salary, and department ID for those employees who earn less than the minimum salary of a department which ID is 70. 
SELECT first_name || ' ' || last_name full_name, salary, department_id
FROM employees
INNER JOIN departments USING(department_id)
WHERE salary < (
  SELECT min(salary)
  FROM employees
  WHERE department_id = 70
);

--SELECT first_name || ' ' || last_name full_name, e.salary, department_id
--FROM employees e
--INNER JOIN departments USING(department_id)
--INNER JOIN (
--  SELECT min(salary) salary
--  FROM employees
--  WHERE department_id = 70
--) s ON e.salary < s.salary;

-- what is better to use ?

--72. the first and last name, salary, and department ID for those employees who earn less than the average salary, and also work at the department where the employee Laura is working as a first name holder. 
SELECT first_name || ' ' || last_name full_name, salary, department_id
FROM employees
INNER JOIN departments USING(department_id)
WHERE salary < ( SELECT avg(salary) FROM employees )
AND department_id = (
    SELECT department_id FROM departments
    INNER JOIN employees USING(department_id)
    WHERE first_name = 'Laura'
);

--73. the city of the employee whose ID 134 and works there.   
SELECT city
FROM locations
INNER JOIN departments USING(location_id)
INNER JOIN employees USING(department_id)
WHERE employee_id = 134;

--74. the the details of those departments which max salary is 7000 or above for those employees who already done one or more jobs.   
SELECT d.department_id, department_name
FROM departments d
INNER JOIN employees e ON d.department_id = e.department_id
INNER JOIN job_history USING(employee_id)
WHERE salary >= 7000;
-- one ore more jobs means that they have something in job history ? if yes it's done

--75. the detail information of those departments which starting salary is at least 8000.  
SELECT department_id, department_name
FROM departments
INNER JOIN employees USING(department_id)
WHERE salary >= 8000;

--76. the full name (first and last name) of manager who is supervising 4 or more employees. 
SELECT first_name || ' ' || last_name full_name
FROM employees e
INNER JOIN (
  SELECT manager_id, count(manager_id) nr_employees
  FROM employees
  WHERE manager_id IS NOT NULL
  GROUP BY manager_id
) m ON e.employee_id = m.manager_id
WHERE nr_employees >= 4;

--77. the details of the current job for those employees who worked as a Sales Representative in the past.   
SELECT job_title
FROM employees e
INNER JOIN job_history h ON e.employee_id = h.employee_id
INNER JOIN jobs j ON j.job_id = h.job_id
WHERE job_title = 'Sales Representative';
-- not sure if correct

--78. all the infromation about those employees who earn second lowest salary of all the employees.
SELECT employee_id, first_name || ' ' || last_name full_name, email, phone_number, salary, hire_date
FROM employees
WHERE salary = (
  SELECT salary
  FROM employees
  ORDER BY salary
  OFFSET 1 ROWS FETCH NEXT 1 ROWS ONLY
);

--79. the details of departments managed by Susan.  
SELECT d.department_id, department_name
FROM departments d
INNER JOIN employees e ON d.manager_id = e.employee_id
WHERE first_name = 'Susan';

--80. the department ID, full name (first and last name), salary for those employees who is highest salary drawar in a department. 
SELECT e.department_id, first_name || ' ' || last_name full_name, e.salary
FROM employees e
INNER JOIN (
  SELECT max(salary) salary, department_id
  FROM employees
  INNER JOIN departments USING(department_id)
  GROUP BY department_id
) m ON m.department_id = e.department_id AND e.salary = m.salary;
